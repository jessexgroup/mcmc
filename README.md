### What is this repository for? ###

The repo is part of the MCMC Focus Group, where we meet regulary to work on the theory and write codes to understand better topics that are related to MCMC
It contains documents and codes on topics that is related to MCMC (Markov Chain Monte Carlo) methods. 

### focusGroups meeting ###

* **2017-10-19:** Investigation of the Phase Space Volume preservation attribute for symplectic integrators such as Leap-Frog, Velocity Verlet etc.

### Who do I talk to? ###

* Can Pervane
* Khaled Maksoud 
* Marley Samways