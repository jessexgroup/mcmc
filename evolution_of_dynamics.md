An attempt to list the time evolution of analysis of dynamical systems. The following questions are in main interest.

* How did we analyse dynamical systems before calculus?
* How did we analyse stochastic dynamical systems?
* Did we start to analyse stochastic dynamical systems after the invention of calculus?
* How did we invent stochastic calculus, how is the transitions made from calculus to stochastic calculus?
* What are the main keywords used in calculus and stochastic calculus?
* What new ideas does stochastic calculus bring over calculus?

# Calculus

According to [Wiki](https://en.wikipedia.org/wiki/Calculus)

"Modern calculus was developed in 17th-century Europe by Isaac Newton and Gottfried Wilhelm Leibniz (independently of each other, first publishing around the same time) but elements of it have appeared in ancient Greece, then (alphabetically) in China and the Middle East, and still later again in medieval Europe and in India."

would be nice if we could get access to the source of the work mentioned in the quote

# Stochastic Calculus

Is not straightforward as calculus to find some resources on the history of stochastic calculus.

on a [wikipedia page](https://en.wikipedia.org/wiki/Stochastic_calculus), they explain there are two flavors of stochastic calculus:

1. Ito calculus
2. Malliavin calculus

So maybe we can find the history of these two flavors.
